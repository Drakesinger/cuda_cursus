#include <omp.h>
#include "MathTools.h"
#include "OmpTools.h"
#include "../02_Slice/00_pi_tools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPforPromotionTab_Ok(int n);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static double piOMPforPromotionTab(int n);
static void syntaxeSimplifier(double* tabSumThread, int n);
static void syntaxeFull(double* tabSumThread, int n);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPforPromotionTab_Ok(int n)
    {
    return isAlgoPI_OK(piOMPforPromotionTab, n, "Pi OMP for promotion tab");
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * De-synchronisation avec PromotionTab
 */
double piOMPforPromotionTab(int n)
    {
    // Get and set the number of threads according to the number of CPU cores.
    const int NB_THREADS = OmpTools::setAndGetNaturalGranularity();

    double dx = 1.0 / double(n);
    // Global sum;
    double sum = 0.0;

    // The table of results of each thread.
    double* ptrTab_Thread = new double[NB_THREADS];

    // Initialize the table.
    for (int i = 0; i <= NB_THREADS; i++)
	{
	ptrTab_Thread[i] = 0.0;
	}

#pragma omp parallel for
    for (int i = 1; i < n; ++i)
	{
	int TID = OmpTools::getTid();
	double xi = i * dx;
	ptrTab_Thread[TID] += fpi(xi);
	sum += fpi(xi);
	}

    // Sequential reduction.
    for (int i = 0; i <= NB_THREADS; i++)
    	{
    	sum += ptrTab_Thread[i];
    	}

    // Delete the allocated table of sums.
    delete[] ptrTab_Thread;

    return sum * dx;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

