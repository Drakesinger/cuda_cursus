#include <omp.h>
#include "OmpTools.h"
#include "../02_Slice/00_pi_tools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPEntrelacerPromotionTab_Ok(int n);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static double piOMPEntrelacerPromotionTab(int n);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPEntrelacerPromotionTab_Ok(int n)
    {
    return isAlgoPI_OK(piOMPEntrelacerPromotionTab, n, "Pi OMP Entrelacer promotionTab");
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * pattern cuda : excellent!
 */
double piOMPEntrelacerPromotionTab(int n)
    {
    // Get and set the number of threads according to the number of CPU cores.
    const int NB_THREADS = OmpTools::setAndGetNaturalGranularity();

    double dx = 1.0 / double(n);
    // Global sum;
    double sum = 0.0;

    // The table of results of each thread.
    double* ptrTab_Thread = new double[NB_THREADS];

#pragma omp parallel
	{

	const int TID = OmpTools::getTid();
	int s = TID;

	double xs;
	double sum_thread = 0.0;

	while (s < n)
	    {
	    xs = s * dx;
	    sum_thread += fpi(xs);

	    // Jump.
	    s += NB_THREADS;
	    }

	ptrTab_Thread[TID] = sum_thread;
	}

    // Reduce the table.
    for (int i = 0; i < NB_THREADS; ++i)
	{
	sum += ptrTab_Thread[i];
	}

    // Delete the allocated table of sums.
    delete[] ptrTab_Thread;

    return sum * dx;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

