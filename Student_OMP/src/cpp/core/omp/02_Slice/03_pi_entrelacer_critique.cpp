#include <omp.h>
#include "OmpTools.h"
#include "../02_Slice/00_pi_tools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPEntrelacerCritical_Ok(int n);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static double piOMPEntrelacerCritical(int n);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiOMPEntrelacerCritical_Ok(int n)
    {
    return isAlgoPI_OK(piOMPEntrelacerCritical, n, "Pi OMP Entrelacer critical");
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

double piOMPEntrelacerCritical(int n)
    {
    // Get and set the number of threads according to the number of CPU cores.
    const int NB_THREADS = OmpTools::setAndGetNaturalGranularity();

    double dx = 1.0 / double(n);
    // Global sum;
    double sum = 0.0;

#pragma omp parallel
	{

	const int TID = OmpTools::getTid();
	int s = TID;

	double xs;
	double sum_thread = 0.0;

	while (s < n)
	    {
	    xs = s * dx;
	    sum_thread += fpi(xs);

	    // Jump.
	    s += NB_THREADS;
	    }

// Start critical section and write to the SUM global variable.
#pragma omp critical (interlacedSumComputation)
	    {
	    sum += sum_thread;
	    }
	}

    return sum * dx;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

