#include "../02_Slice/00_pi_tools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiSequentiel_OK(int n);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

static double piSequentiel(int n);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool isPiSequentiel_OK(int n)
    {
    return isAlgoPI_OK(piSequentiel, n, "Pi Sequentiel");
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

double piSequentiel(int n)
    {
    double dx = 1.0 / double(n);

    double sum = 0;
    for (int i = 1; i < n; ++i)
	{
	double xi = i * dx;
	sum += fpi(xi);
	}
    sum *= dx;

    return sum;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

