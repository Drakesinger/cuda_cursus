#pragma once

#include "cudaTools.h"
#include "MathTools.h"

#include "Variateur_GPU.h"
#include "Animable_I_GPU.h"

#include "Sphere.h"
using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Raytracer: public Animable_I<uchar4>
    {
	/*--------------------------------------*\
	|*		Constructor		*|
	 \*-------------------------------------*/

    public:

	Raytracer(const Grid& grid, uint w, uint h, float dt, const DomaineMath& domaineMath, int nrSpheres, float padding);

// Destructor
	virtual ~Raytracer(void);

	/*--------------------------------------*\
	 |*		Methods		*|
	 \*-------------------------------------*/

    private:

	void generateSpheres(Sphere* tabSpheres, uint w, uint h, int nrSpheres, float padding);

	float generateRandomNumber(float min, float max);

    public:

	/*-------------------------*\
	|*   Override Animable_I   *|
	 \*------------------------*/

	/**
	 * Call periodicly by the api
	 */
	virtual void process(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath& domaineMath);

	/**
	 * Call periodicly by the api
	 */
	virtual void animationStep();

	/*--------------------------------------*\
	 |*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int nrSpheres;
	float padding;

	// Tools
	Variateur<int> variateurAnimation;
	Sphere* ptrHostArraySpheres;
	Sphere* ptrDeviceArraySpheres;
	size_t size;

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
