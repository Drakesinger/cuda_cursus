#include "Sphere.h"

#include <iostream>
#include "RaytracingGM.h"

using std::cout;
using std::endl;

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void raytracingDeviceGM(uchar4* ptrDevPixels, uint w, uint h, float t, const DomaineMath domaineMath, Sphere* ptrDevSpheres, int n);

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/


/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

RaytracingGM::RaytracingGM(uint w, uint h, float dt, uint nMin, uint nMax, const DomaineMath& domaineMath, const Grid& grid) :
	Animable_I<uchar4>(grid, w, h, "RaytracingGM_Cuda_rgba_uchar4", domaineMath), variateurAnimation(Interval<float>(nMin, nMax), dt)
    {
    // Tools
    this->t = 0; // protected dans super classe Animable


    ptrData = new Sphere[n];
    ptrDevData = NULL;

    size = n * sizeof(Sphere);

    fillSpheres(ptrData, n, w, h);

    HANDLE_ERROR(cudaMalloc((void**) &ptrDevData, size));

    int initialValue = 0; //valeur en octet
    HANDLE_ERROR(cudaMemset (ptrDevData, initialValue, size));

    //
    //Copie Host->Device
    HANDLE_ERROR(cudaMemcpy(ptrDevData, ptrData, size, cudaMemcpyHostToDevice));

    }

RaytracingGM::~RaytracingGM(void)
    {
    HANDLE_ERROR(cudaFree(ptrDevData));

    delete ptrData;
    }

/**
 * Fill spheres tab
 */
void RaytracingGM::fillSpheres(Sphere* tabSpheres, int n, int w, int h)
    {

    const int precision = 100;

    int littleSize = w;
    if(h<littleSize)
	{
	littleSize = h;
	}

    const int bord = littleSize/5;

    for (int i = 0; i < n; i++)
	{

	float rayon = randBetweenNumbers(20, w/10, precision);

	float3 centre;
	centre.x = randBetweenNumbers(0+bord, w-bord, precision);
	centre.y = randBetweenNumbers(bord, h-bord, precision);
	centre.z = randBetweenNumbers(10, 2 * w, precision);

	float hue = randBetweenNumbers(0, 1, precision);

	tabSpheres[i] = Sphere(centre, rayon, hue);
	}
    }

/**
 * Override (code entrainement cuda)
 */
void RaytracingGM::process(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath)
    {

    Device::lastCudaError("raytracing rgba uchar4 (before)"); // facultatif, for debug only, remove for release

    raytracingDeviceGM<<<dg, db>>>(ptrTabPixels, w, h, t, domaineMath, ptrDevData, n);

    Device::synchronize();
    Device::lastCudaError("raytracing rgba uchar4 (after)"); // facultatif, for debug only, remove for release

    }

/**
 * Override
 */
void RaytracingGM::animationStep()
    {
    this->t = variateurAnimation.varierAndGet();
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * Generate random float number
 */
float RaytracingGM::randBetweenNumbers(int min, int max, int precision)
    {
    return (float) (rand() % (max * precision - min * precision) + min * precision) / precision;
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

