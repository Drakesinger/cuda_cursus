#pragma once

#include "Variateur_GPU.h"
#include "Animable_I_GPU.h"
#include "Sphere.h"

using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class RaytracingGM: public Animable_I<uchar4>
    {

	/*--------------------------------------*\
	 |*		Constructeur		*|
	 \*-------------------------------------*/

    public:

	RaytracingGM(uint w, uint h, float dt, uint nMin, uint nMax, const DomaineMath& domaineMath, const Grid& grid);

	virtual ~RaytracingGM(void);

	/*--------------------------------------*\
	 |*		Methode			*|
	 \*-------------------------------------*/

	/*-------------------------*\
	|*   Override Animable_I   *|
	 \*------------------------*/

	/**
	 * Call periodicly by the api
	 */
	virtual void process(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath);

	/**
	 * Call periodicly by the api
	 */
	virtual void animationStep();



    private:

	/**
	 * Generate random float number
	 */
	float randBetweenNumbers(int min, int max, int precision);

	/**
	 * Fill spheres tab
	 */
	void fillSpheres(Sphere* tabSpheres, int n, int w, int h);

	/*--------------------------------------*\
	|*		Attribut		*|
	 \*-------------------------------------*/

	// Inputs
	// rien

	// Tools
	Variateur<float> variateurAnimation;

	Sphere* ptrData;
	Sphere* ptrDevData;
	size_t size;
	static const int n = 800; //nombre de spheres, limite SM

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 /*----------------------------------------------------------------------*/
