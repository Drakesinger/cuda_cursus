#include "Raytracer.h"

#include <iostream>
#include <assert.h>

#include <cstdlib>
#include <ctime>

#include "Device.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

// Runs on both the host and the device (gpu).
// Kernel
__global__ void raytracerDevice(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath domaineMath, float t, Sphere* ptrDeviceSphereArray, int nrSpheres);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*-------------------------*\
 |*	Constructor	    *|
 \*-------------------------*/

Raytracer::Raytracer(const Grid& grid, uint w, uint h, float dt, const DomaineMath& domaineMath, int nrSpheres, float padding) :
	Animable_I<uchar4>(grid, w, h, "Raytracer_Cuda_RGBA_uchar4", domaineMath), variateurAnimation(Interval<int>(0, 100), dt)
//, nrSpheres(nrSpheres), padding(padding)
    {
    // Seed the bad rng.
    srand(static_cast<unsigned>(time(0)));

    // Inputs
    this->nrSpheres = nrSpheres;
    this->padding = padding;

    // Tools
    this->t = 0;  // protected dans Animable

    // Ready the memory.
    ptrHostArraySpheres = new Sphere[nrSpheres];
    ptrDeviceArraySpheres = NULL;

    // Define the size of the array to share.
    size = nrSpheres * sizeof(Sphere);

    // Generate the spheres
    generateSpheres(ptrHostArraySpheres, w, h, nrSpheres, padding);

    // Only do this once and then delete at the end.

    // Now allocate the memory.
    HANDLE_ERROR(cudaMalloc((void**) &ptrDeviceArraySpheres, size));
    Device::lastCudaError("cudaMalloc after");

    // Initialize the memory.
    HANDLE_ERROR(cudaMemset(ptrDeviceArraySpheres, 0, size));
    Device::lastCudaError("cudaMemset after");

    // Copy from Host to Device.
    HANDLE_ERROR(cudaMemcpy(ptrDeviceArraySpheres, ptrHostArraySpheres, size, cudaMemcpyHostToDevice));

    Device::lastCudaError("cudaMemcpy after");
    }

Raytracer::~Raytracer()
    {
    // Delete the DEVICE memory.
    HANDLE_ERROR(cudaFree(ptrDeviceArraySpheres));

    // Delete our table of spheres to free up memory on the HOST (CPU(.
    delete[] ptrHostArraySpheres;

    // Not sure if this works on linux. TODO verify
    //tabSpheres = nullptr;
    }

/*-------------------------*\
 |*	Methods		    *|
 \*-------------------------*/

/*-------------------------*\
	|*		Private 						 *|
 \*-------------------------*/

// Generate spheres in the image domain.
void Raytracer::generateSpheres(Sphere* tabSpheres, uint w, uint h, int nrSpheres, float padding)
    {
    // Navigate the sphere array and generate random values for the sphere.
    for (int iSphere = 0; iSphere < nrSpheres; iSphere++)
	{
	// Generate our randoms.
	float radius = generateRandomNumber(20.0f, w / 10.0f);
	float x = generateRandomNumber(padding, w - padding - w/10.0f);
	float y = generateRandomNumber(padding, h - padding - w/10.0f);
	float z = generateRandomNumber(10.0f, 2.0f * w);
	float hue = generateRandomNumber(0.0f, 1.0f);

	// Create the center. Apparently this is how to do it.
	float3 center;
	center.x = x;
	center.y = y;
	center.z = z;
	// Or faster apparently: center = make_float3(x, y, z);

	// Initialize the sphere.
	//Sphere sphere = Sphere(center, radius, hue);
	// Add it to the array.
	tabSpheres[iSphere] = Sphere(center, radius, hue);

	// Verify.
	//std::cout << iSphere << " - " << sphere.toString();
	}
    }

float Raytracer::generateRandomNumber(float min, float max)
    {
    return (min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min))));
    }

/**
 * Override
 * Call periodicly by the API
 */
void Raytracer::process(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath& domaineMath)
    {
    Device::lastCudaError("raytracer rgba uchar4 (before)"); // facultatif, for debug only, remove for release

    raytracerDevice<<<dg,db>>>(ptrDevPixels, w, h, domaineMath, (float)t, ptrHostArraySpheres, nrSpheres);

    Device::synchronize();
    //std::cout << "Synchronized threads." << std::endl;
    Device::lastCudaError("raytracer rgba uchar4 (after)"); // facultatif, for debug only, remove for release
    }

/**
 * Override
 * Call periodicly by the API
 */
void Raytracer::animationStep()
    {
    this->t = variateurAnimation.varierAndGet(); // in [0,2pi]
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
