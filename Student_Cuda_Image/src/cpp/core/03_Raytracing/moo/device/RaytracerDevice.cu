#include "Indice2D.h"
#include "cudaTools.h"
#include "Device.h"

#include "Sphere.h"

//#include "RaytracerMath.h"

#include "ColorTools_GPU.h"
#include "IndiceTools_GPU.h"
#include "DomaineMath_GPU.h"

#include <stdio.h>
using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void raytracerDevice(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath domaineMath, float t, Sphere* ptrDeviceSphereArray, int nrSpheres);
__device__ void workPixel(uchar4* ptrColorIJ, int i, int j, float t, const DomaineMath domaineMath, Sphere* ptrDeviceSphereArray, int nrSpheres);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__global__ void raytracerDevice(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath domaineMath, float t, Sphere* ptrDeviceSphereArray, int nrSpheres)
    {
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;

    int pixelI = 0; // in [0,h[
    int pixelJ = 0; // in [0,w[

    int s = TID;
    while (s < WH)
	{
	IndiceTools::toIJ(s, w, &pixelI, &pixelJ); // update (pixelI, pixelJ)

	workPixel(&ptrDevPixels[s], pixelI, pixelJ, t, domaineMath, ptrDeviceSphereArray, nrSpheres);

	s += NB_THREAD;
	}

    }

__device__ void workPixel(uchar4* ptrColorIJ, int i, int j, float t, const DomaineMath domaineMath, Sphere* ptrDeviceSphereArray, int nrSpheres)
    {

    // Color black.
    float h = 0;
    float s = 0;
    float b = 0;

    float hm = 0;
    float bm = 0;

    double x = 0.0;
    double y = 0.0;

    domaineMath.toXY(i, j, &x, &y); // fill (x,y) from (i,j)

    float min = -100000.f;

    float2 xySol;
    xySol.x = float(x);
    xySol.y = float(y);

    int iSphere = 0;

    do
	{
	// The sphere we check.
	Sphere sphere = ptrDeviceSphereArray[iSphere];

	float hCarre = ptrDeviceSphereArray[iSphere].hCarre(xySol);

	if (sphere.isEnDessous(hCarre))
	    {
	    float dz = sphere.dz(hCarre);
	    float distance = sphere.distance(dz);

	    if (distance < min)
		{
		min = distance;
		hm = sphere.hue(t);
		bm = sphere.brightness(dz);
		}
	    }

	iSphere++;
	}
    while (iSphere < nrSpheres);

    if (hm >= 0 && bm >= 0)
	{
	// Color according to the sphere's color.
	ColorTools::HSB_TO_RVB(hm, 1.0f, bm, ptrColorIJ);
	}
    else
	{
	// Color in black.
	ColorTools::HSB_TO_RVB(h, s, b, ptrColorIJ);
	}

    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
