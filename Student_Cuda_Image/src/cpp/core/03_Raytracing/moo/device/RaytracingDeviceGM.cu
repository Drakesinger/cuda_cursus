#include "cudaTools.h"
#include "Device.h"

#include "Sphere.h"

#include "DomaineMath_GPU.h"
#include "IndiceTools_GPU.h"
#include "Indice2D.h"

#include "ColorTools_GPU.h"

using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

__global__ void raytracingDeviceGM(uchar4* ptrDevPixels, uint w, uint h, float t, const DomaineMath domaineMath, Sphere* ptrDevSpheres, int n);
__device__ void workPixelGM(uchar4* ptrColorIJ, int i, int j, float t, const DomaineMath domaineMath, Sphere* ptrDevSpheres, int n);



/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

__global__ void raytracingDeviceGM(uchar4* ptrDevPixels, uint w, uint h, float t, const DomaineMath domaineMath, Sphere* ptrDevSpheres, int n)
    {
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;

    //pattern entrelacement
    int s = TID; // in [0,...

    int i;
    int j;
    while (s < WH)
	{
	IndiceTools::toIJ(s, w, &i, &j); // s[0,W*H[ --> i[0,H[ j[0,W[

	workPixelGM(&ptrDevPixels[s], i, j, t, domaineMath, ptrDevSpheres, n);

	s += NB_THREAD;
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * i in [0,h[
 * j in [0,w[

 */
__device__ void workPixelGM(uchar4* ptrColorIJ, int i, int j, float t, const DomaineMath domaineMath, Sphere* ptrDevSpheres, int n)
    {
    // (i,j) domaine ecran dans N2
    // (x,y) domaine math dans R2

    double x;
    double y;
    domaineMath.toXY(i, j, &x, &y); // fill (x,y) from (i,j)

    float distanceEnDessous = NULL;
    float dzEnDessous = NULL;
    int iSphereEnDessous = NULL;

    float2 xySol;
    xySol.x = (float) x;
    xySol.y = (float) y;

    for (int i = 0; i < n; i++)
	{
	float hCarre = ptrDevSpheres[i].hCarre(xySol);

	if (ptrDevSpheres[i].isEnDessous(hCarre))
	    {
	    float dz = ptrDevSpheres[i].dz(hCarre);
	    float distance = ptrDevSpheres[i].distance(dz);

	    if(iSphereEnDessous == NULL || ( distance < distanceEnDessous && distanceEnDessous != NULL ) )
		{
		distanceEnDessous = distance;
		dzEnDessous = dz;
		iSphereEnDessous = i;
		}
	    }
	}

    if(iSphereEnDessous != NULL)
	{
	float h = ptrDevSpheres[iSphereEnDessous].hue(t);
	float s = 1;
	float b = ptrDevSpheres[iSphereEnDessous].brightness(dzEnDessous);

	ptrColorIJ->x = h;
	ptrColorIJ->y = s;
	ptrColorIJ->z = b;

	ptrColorIJ->w = 1;

	ColorTools::HSB_TO_RVB(h,s,b, ptrColorIJ);
	}
    else
	{
	ptrColorIJ->x = 0;
	ptrColorIJ->y = 0;
	ptrColorIJ->z = 0;

	ptrColorIJ->w = 1;
	}

    }


/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

