#include "RaytracingProvider.h"
#include "RaytracingGM.h"
#include "Raytracer.h"

#include "MathTools.h"
#include "Grid.h"

#include "DomaineMath_GPU.h"
using gpu::DomaineMath;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Override
 */
Animable_I<uchar4>* RaytracingProvider::createAnimable()
    {


    // Animation;
    float dt = 0.01;//2 * PI / 8000;
    int nMin = 0;
    int nMax = 100;

    int nrSpheres = 5;
    float padding = 20.0f;

    // Dimension
    int dw = 16 * 60;
    int dh = 16 * 60;

    DomaineMath domaineMath = DomaineMath(0, 0, dw, dh);

    // Grid Cuda
    dim3 dg = dim3(32, 1, 1);  		// disons a optimiser, depend du gpu
    dim3 db = dim3(448, 1, 1);   	// disons a optimiser, depend du gpu
    Grid grid(dg, db);

    //return new RaytracingGM(dw, dh, dt, nMin, nMax, domaineMath, grid);
    return new Raytracer(grid, dw, dh, dt, domaineMath, nrSpheres, padding);
    }

/**
 * Override
 */
Image_I* RaytracingProvider::createImageGL(void)
    {
    ColorRGB_01 colorTexte(0, 0, 0); // black
    return new ImageAnimable_RGBA_uchar4(createAnimable(), colorTexte);
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
