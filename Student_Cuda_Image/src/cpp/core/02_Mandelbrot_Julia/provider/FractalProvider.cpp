#include "FractalProvider.h"
#include "Fractal.h"
#include "Julia.h"

#include "MathTools.h"
#include "Grid.h"

#include "DomaineMath_GPU.h"
using gpu::DomaineMath;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Surcharge		*|
 \*-------------------------------------*/

/**
 * Override
 */
Animable_I<uchar4>* FractalProvider::createAnimable(void)
    {
    DomaineMath domaineMathMandel = DomaineMath(-1.3968, -0.03362, -1.3578, 0.0013973); // Madelbrot
    DomaineMath domaineMathJulia = DomaineMath(-1.5, -1.5, 1.5, 1.5); // julia

    // Animation
    float dt = 1;

    // Dimension
    int dw = 16 * 60;
    int dh = 16 * 60;

    // Limit.
    int Nm = 102; // Mandelbrot
    int Nj = 50; // Julia


    // Grid Cuda
    dim3 dg = dim3(16, 2, 1);  	// disons a optimiser, depend du gpu
    dim3 db = dim3(32, 24, 1);   	// disons a optimiser, depend du gpu
    Grid grid(dg, db);

    //Fractal* fractale = new Julia(grid, dw, dh, dt, 0.25, 0.789, Nj, domaineMathJulia);
    Fractal* fractale = new Fractal(grid, dw, dh, dt, Nm, domaineMathMandel);

    return fractale;
    }

/**
 * Override
 */
Image_I* FractalProvider::createImageGL(void)
    {
    ColorRGB_01 colorTexte(0, 1, 0); // green
    return new ImageAnimable_RGBA_uchar4(createAnimable(), colorTexte);
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
