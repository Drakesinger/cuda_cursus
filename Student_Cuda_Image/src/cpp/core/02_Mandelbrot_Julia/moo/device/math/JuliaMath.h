#pragma once

#include <math.h>
#include "MathTools.h"

//#include "Calibreur_GPU.h"
#include "ColorTools_GPU.h"
using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class JuliaMath
    {
	/*--------------------------------------*\
	|*		Constructeur		*|
	 \*-------------------------------------*/

    public:
	
	__device__ JuliaMath(uint w,float c_re, float c_im)
	    {
	    this->c_re = c_re;
	    this->c_im = c_im;
	    this->dim2 = w / 2;
	    }
	
	// constructeur copie: pas besoin car pas attribut ptr
	__device__
	virtual ~JuliaMath(void)
	    {
	    // rien
	    }
	
	/*--------------------------------------*\
	|*		Methode			*|
	 \*-------------------------------------*/

    public:
	__device__
	void computeGeometricSuite(uchar4* ptrColorIJ, double x, double y, int N)
	    {
	    double a = x;
	    double b = y;
	    int k = 0;
	    
	    do
		{
		double a_copy = a;
		a = (a * a - b * b) + c_re;
		b = 2 * a_copy * b + c_im;
		
		if (a * a + b * b > 4.0)
		    {
		    colorXY(ptrColorIJ, determineProRata(k, N));
		    break;
		    }
		
		colorXYBlack(ptrColorIJ);
		
		k++;
		}
	    while (k <= N);
	    }
	
    private:
	__device__
	float determineProRata(int s, int N)
	    {
	    return float(s) / float(N);
	    }
	__device__
	void colorXYBlack(uchar4* ptrColorIJ)
	    {
	    // The pixel @ (x,y) belongs to the fractal.
	    // We need to color it in black.
	    
	    ptrColorIJ->x = 0;
	    ptrColorIJ->y = 0;
	    ptrColorIJ->z = 0;
	    
	    ptrColorIJ->w = 255; // Opaque.
	    }
	__device__
	void colorXY(uchar4* ptrColorIJ, float hue)
	    {
	    // The pixel @ (x,y) does not belong to the fractal.
	    // It's color depends on the 1st k<=N for which norm(z_k) > 2
	    
	    ColorTools::HSB_TO_RVB(hue, ptrColorIJ);
	    }
	
	/*--------------------------------------*\
	|*		Attribut			*|
	 \*-------------------------------------*/

    private:
	// Inputs
	float c_re;
	float c_im;
	
	// Tools
	double dim2;
    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
