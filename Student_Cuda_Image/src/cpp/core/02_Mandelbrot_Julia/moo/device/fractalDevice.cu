#include "Indice2D.h"
#include "cudaTools.h"
#include "Device.h"

#include "FractalMath.h"

#include "IndiceTools_GPU.h"
#include "DomaineMath_GPU.h"
using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/
// Code called from the host (CPU).
__global__ void fractal(uchar4* ptrDevPixels, uint w, uint h, int N, DomaineMath domaineMath);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/
// Code called on the device (GPU).
__device__ static void workPixel(uchar4* ptrColorIJ, int i, int j, DomaineMath* domaineMath, FractalMath* ptrFractalMath, int N);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void fractal(uchar4* ptrDevPixels, uint w, uint h, int N, DomaineMath domaineMath)
    {

    // Start up our math object.
    FractalMath fractalMath = FractalMath(w, N);

    const int WH = w * h;

    // The system has initiated the threads so this info is now known. Outside of the parallel region, it is not.
    const int NB_THREAD = Indice2D::nbThread(); // dans region parallel

    const int TID = Indice2D::tid();
    int s = TID; // in [0,...

    // Image domain.
    int pixelI; // in [0,h[
    int pixelJ; // in [0,w[

    while (s < WH)
	{
	IndiceTools::toIJ(s, w, &pixelI, &pixelJ);   // s[0,W*H[ --> i[0,H[ j[0,W[

	workPixel(&ptrDevPixels[s], pixelI, pixelJ, &domaineMath, &fractalMath, N);

	s += NB_THREAD;
	}

    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * i in [0,h[
 * j in [0,w[
 *
 * code commun a:
 * 	- entrelacementOMP
 * 	- forAutoOMP
 */
__device__ void workPixel(uchar4* ptrColorIJ, int i, int j, DomaineMath* domaineMath, FractalMath* ptrFractalMath, int N)
    {
    // (i,j) domaine ecran dans N2
    // (x,y) domaine math dans R2

    // Fractal domain.
    double x;
    double y;

    domaineMath->toXY(i, j, &x, &y); // fill (x,y) from (i,j)

    ptrFractalMath->computeGeometricSuite(ptrColorIJ, x, y, N);
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

