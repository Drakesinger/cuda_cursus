#pragma once


#include "Fractal.h"

#include "cudaTools.h"
#include "MathTools.h"

#include "Variateur_GPU.h"
#include "Animable_I_GPU.h"
using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Julia: public Fractal
    {
	
	/*-------------------------------------*\
	 |*		Constructor		*|
	 \*-------------------------------------*/

    public:
	
	Julia(const Grid& grid, uint w, uint h, float dt, float c_re, float c_im, int N, const DomaineMath& domaineMath);
	virtual ~Julia(void);

    public:
	
	/*-------------------------*\
    	|*   Override Animable_I   *|
	 \*------------------------*/

	/**
	 * Call periodicly by the api
	 */
	virtual void process(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath& domaineMath);

	/**
	 * Call periodicly by the api
	 */
	virtual void animationStep();

	/*--------------------------------------*\
    	|*		Attribut		*|
	 \*-------------------------------------*/

    private:
	
	// Inputs
	double dt;
	int N;
	float c_im;
	float c_re;

	// Tools
	Variateur<int> variateurAnimation;
	
    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
