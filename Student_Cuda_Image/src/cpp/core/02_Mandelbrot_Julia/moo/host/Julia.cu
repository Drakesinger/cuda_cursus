#include "Julia.h"

#include <iostream>
//#include <assert.h>

#include "Device.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

__global__ void fractalJulia(uchar4* ptrDevPixels, uint w, uint h, int N, DomaineMath domaineMath, float c_re, float c_im);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

Julia::Julia(const Grid& grid, uint w, uint h, float dt, float C_real, float C_imaginary, int N, const DomaineMath& domaineMath) :
	Fractal(grid, w, h, dt, N, domaineMath), variateurAnimation(Interval<int>(20, 100), 1)
    {
    // Input
    this->dt = dt;  // animation
    this->N = N;
    this->c_im = C_imaginary;
    this->c_re = C_real;

    // Tools
    this->t = 0;					// protected dans super classe Animable
    }

Julia::~Julia(void)
    {
    // Nothing.
    }

void Julia::animationStep()
    {
    this->t += dt + variateurAnimation.varierAndGet();
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/


void Julia::process(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath& domaineMath)
    {
    Device::lastCudaError("fractal rgba uchar4 (before)"); // facultatif, for debug only, remove for release

    // The interleaved process is here. Took it out and inserted in fractalDevice (it runs on the gpu processors).
    fractalJulia<<<dg,db>>>(ptrDevPixels,w,h,N,domaineMath, c_re, c_im);

    Device::synchronize();
    Device::lastCudaError("fractal rgba uchar4 (before)"); // facultatif, for debug only, remove for release
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
