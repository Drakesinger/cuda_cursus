#pragma once

#include "cudaTools.h"
#include "MathTools.h"


#include "Variateur_GPU.h"
#include "Animable_I_GPU.h"
using namespace gpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Fractal: public Animable_I<uchar4>
    {

	/*-------------------------------------*\
	 |*		Constructor		*|
	 \*-------------------------------------*/

    public:
	Fractal(const Grid& grid,uint w, uint h, float dt, int N, const DomaineMath& domaineMath);
	virtual ~Fractal(void);

	/*--------------------------------------*\
	 |*		Methodes		*|
	 \*-------------------------------------*/

    public:

	/*-------------------------*\
    	|*   Override Animable_I   *|
	 \*------------------------*/

	/**
	 * Call periodicly by the api
	 */
	virtual void process(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath& domaineMath);

	/**
	 * Call periodicly by the api
	 */
	virtual void animationStep();

	/*--------------------------------------*\
    	|*		Attribut		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	double dt;
	//int N;

    protected:
	int N;

    private:
	// Tools
	Variateur<int> variateurAnimation;

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
