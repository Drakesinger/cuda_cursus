#include "Fractal.h"

#include <iostream>
//#include <assert.h>

#include "Device.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

__global__ void fractal(uchar4* ptrDevPixels, uint w, uint h, int N, DomaineMath domaineMath);
//__global__ void fractal(uchar4* ptrDevPixels, uint w, uint h, float t, int N, const DomaineMath& domaineMath);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*-------------------------*\
 |*	Constructeur	    *|
 \*-------------------------*/

Fractal::Fractal(const Grid& grid, uint w, uint h, float dt, int N, const DomaineMath& domaineMath) :
	Animable_I<uchar4>(grid, w, h, "Fractal_CUDA_rgba_uchar4", domaineMath), variateurAnimation(Interval<int>(20, 100), 1)
    {
    // Input
    this->dt = dt;  // animation
    this->N = N;

    // Tools
    this->t = 0;					// protected dans super classe Animable
    }

Fractal::~Fractal(void)
    {
    // Nothing.
    }

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Override
 */
void Fractal::animationStep()
    {
    this->t += dt + variateurAnimation.varierAndGet();
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

void Fractal::process(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath& domaineMath)
    {
    Device::lastCudaError("fractal rgba uchar4 (before)"); // facultatif, for debug only, remove for release

    // The interleaved process is here. Took it out and inserted in fractalDevice (it runs on the gpu processors).
    //fractal<<<dg,db>>>(ptrDevPixels,w,h,t,N,domaineMath);
    fractal<<<dg,db>>>(ptrDevPixels,w,h,(float)t,domaineMath);

    Device::synchronize();
    Device::lastCudaError("fractal rgba uchar4 (before)"); // facultatif, for debug only, remove for release
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
