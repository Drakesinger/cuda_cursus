#pragma once

#include "cudaTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Slice
    {
	/*--------------------------------------*\
	|*		Constructor		*|
	 \*-------------------------------------*/

    public:

	Slice(int nrSlices);

	virtual ~Slice(void);

	/*--------------------------------------*\
	|*		Methodes		*|
	 \*-------------------------------------*/

    public:

	void run();
	float getPi();

    private:
	void memoryManagement();

	/*--------------------------------------*\
	|*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int nrSlices;

	// Outputs
	float pi;

	// Tools
	dim3 dg;
	dim3 db;

	int nSM;

	// Array that will store the result on the device.
	float* ptrDevResult;
	// Size of shared array in SHARED MEMORY.
	size_t sizeSM;
	// Size of PI in SHARED MEMORY.
	size_t sizePI;

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
