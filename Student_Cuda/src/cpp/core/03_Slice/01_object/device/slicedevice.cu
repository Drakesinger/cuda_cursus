#include "Indice2D.h"
#include "cudaTools.h"
#include "reduction_tools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void kernelPI(float* ptrDevResult, int nrSlices, int nSM);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void initializeSM(float* tabSM, int nSM);
__device__ void work(float* tabSM, int nrSlices);
__device__ float fpi(float x);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * output : void required !!
 */
__global__ void kernelPI(float* ptrDevResult, int nrSlices, int nSM)
    {
    extern __shared__ float tabSM[];
    // Initialize ReductionTools if needed.

    //Initialize Shared Memory.
    initializeSM(tabSM, nSM);
    __syncthreads();
    work(tabSM, nrSlices);
    __syncthreads();
    // Call to ReuctionTools.reduce<float>
    ReductionTools<float>::reduce(tabSM,ptrDevResult,nSM);
    //__syncthreads();
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void work(float* tabSM, int nrSlices)
    {
    const int NB_THREADS = Indice2D::nbThread();
    const int TID = Indice2D::tid();
    const int TID_LOCAL = Indice2D::tidLocal();

    int s = TID;

    float dx = 1.0f / float(nrSlices);

    // Global sum;
    float sum_thread = 0.0f;

    while (s < nrSlices)
	{
	float xs = s * dx;
	sum_thread += fpi(xs);

	// Jump.
	s += NB_THREADS;
	}

    tabSM[TID_LOCAL] = sum_thread;
    }

__device__ float fpi(float x)
    {
    return 4.0f / (1.0f + x * x);
    }

__device__ void initializeSM(float* tabSM, int nSM)
    {
    // Put all SharedMemory to 0.
    const int NB_THREAD_BLOCK = Indice2D::nbThreadBlock();
    const int TID_LOCAL = Indice2D::tidLocal();
    int s = TID_LOCAL;

    while (s < nSM)
	{
	tabSM[s] = 0.0f;
	s += NB_THREAD_BLOCK;
	}
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

