#pragma once

#include "curand_kernel.h"
#include "cudaTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Histogram
    {
	/*--------------------------------------*\
	|*		Constructor		*|
	 \*-------------------------------------*/

    public:

	Histogram(int n);

	virtual ~Histogram(void);

	/*--------------------------------------*\
	|*		Methodes		*|
	 \*-------------------------------------*/

    public:

	void run();
	int* getHistogram();

    private:
	void memoryManagement();
	int randomMinMax(int min, int max);
	void generateNumbers();

	/*--------------------------------------*\
	|*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int n;

	// Outputs
	int* tabHistogram;

	// Tools
	dim3 dg;
	dim3 db;

	int minValue;
	int maxValue;

	// Number of slots in SharedMemory.
	int nSM;

	int numberOfThreads;

	// Array of numbers to randomize and turn into histogram.
	int* ptrTabInputNumbers;

	// Array that will store the result on the device.
	int* ptrDevTabResult;

	// Array that will store the random n numbers on the device.
	int* ptrDevTabInput;

	// Size of shared array in SHARED MEMORY.
	size_t sizeSM;

	// Size of the array of numbers.
	size_t sizeTabInput;

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
