#include <iostream>
#include <math.h>
#include <limits.h>

#include "MathTools.h"

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

#include "host/Histogram.h"

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool useHistogram(void);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

bool useHistogram()
    {
    // Total!
    int n = 256;


    bool isOk = true;

    // Determine if the hardware is working in parallel:
    // nvidia-smi --loop=1
    Histogram histogram(n);
    histogram.run();

    int* estimatedPi = histogram.getHistogram();
    //cout << "Histogram Table: " << estimatedPi << endl;

    for(int i = 0; isOk && (i < 255); i++)
	{
	//isOk = (estimatedPi[i] == estimatedPi[i+1]);
	cout << "[" << estimatedPi[i] << "]";
	}
    //cout << "[" << estimatedPi[255] << "]";

    cout << endl;

    //isOk &= MathTools::isEquals(estimatedPi, PI_FLOAT, 1e-6f);

    return isOk;
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

