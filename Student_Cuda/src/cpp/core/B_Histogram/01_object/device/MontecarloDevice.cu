#include "Indice2D.h"
#include "cudaTools.h"
#include "reduction_tools.h"

#include <curand_kernel.h>

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void kernelMontecarlo(float* ptrDevResult, curandState* ptrDevTabGenerator, int nrArrows, int nSM, float yMax);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void initializeSMM(float* tabSM, int nSM);
__device__ void reduceIntraThread(float* tabSM, curandState* ptrDevTabGenerator, int nrArrows, float yMax);
__device__ float fpiM(float x);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * output : void required !!
 */
__global__ void kernelMontecarlo(float* ptrDevResult, curandState* ptrDevTabGenerator, int nrArrows, int nSM, float yMax)
    {
    extern __shared__ float tabSM[];

    //Initialize Shared Memory.
    //initializeSMM(tabSM, nSM);

    __syncthreads();
    reduceIntraThread(tabSM, ptrDevTabGenerator, nrArrows, yMax);
    __syncthreads();

    // Call to ReuctionTools.reduce<float>
    ReductionTools<float>::reduce(tabSM, ptrDevResult, nSM);

    //__syncthreads();
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void reduceIntraThread(float* tabSM, curandState* ptrDevTabGenerator, int nrArrows, float yMax)
    {
    const int NB_THREADS = Indice2D::nbThread();
    const int TID = Indice2D::tid();
    const int TID_LOCAL = Indice2D::tidLocal();

    // Get the random state for this thread.
    curandState state = ptrDevTabGenerator[TID];

    // Global sum;
    // Number of dots under the curve.
    float sum_thread = 0.0f;
    int nbArrowsperThread = nrArrows / NB_THREADS;

    for (int i = 0; i < nbArrowsperThread; i++)
	{
	float x = curand_uniform(&state);
	float y = curand_uniform(&state) * yMax;

	// Check if y is smaller than the function.
	float fx = fpiM(x);

	if (y < fx)
	    {
	    sum_thread += 1;
	    }
	}

    tabSM[TID_LOCAL] = sum_thread;
    }

__device__ float fpiM(float x)
    {
    return 4.0f / (1.0f + x * x);
    }

__device__ void initializeSMM(float* tabSM, int nSM)
    {
    // Put all SharedMemory to 0.
    const int NB_THREAD_BLOCK = Indice2D::nbThreadBlock();
    const int TID_LOCAL = Indice2D::tidLocal();
    int s = TID_LOCAL;

    while (s < nSM)
	{
	tabSM[s] = 0.0f;
	s += NB_THREAD_BLOCK;
	}
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

