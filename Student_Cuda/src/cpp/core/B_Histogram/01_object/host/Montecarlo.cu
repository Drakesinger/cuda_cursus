#include <iostream>

#include "Device.h"
#include "Montecarlo.h"

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

__global__ void kernelMontecarlo(float* ptrDevResult, curandState* ptrDevTabGenerator, int nrArrows, int nSM, float yMax);
extern __global__ void setup_kernel_rand(curandState* tabDevGenerator, int deviceId);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Constructeur			*|
 \*-------------------------------------*/

Montecarlo::Montecarlo(int numberOfRandomPoints, float yMax) :
	numberOfRandomPoints(numberOfRandomPoints), yMax(yMax)
    {
    this->nrArrowsUnderCurve = 0.0f;
    sizePI = sizeof(float);

    this->numberOfDevices = Device::getDeviceCount();

    this->tabMontecarlo = new float[numberOfDevices];
    this->ptrDevTabResult = new float*[numberOfDevices];
    this->ptrDevTabGenerator = new curandState*[numberOfDevices];

    // Grid.
    this->dg = dim3(32, 16, 1); // disons, a optimiser selon le gpu
    this->db = dim3(512, 1, 1); // disons, a optimiser selon le gpu

    // Testing.
    //this->dg = dim3(2, 1, 1); // disons, a optimiser selon le gpu
    //this->db = dim3(4, 1, 1); // disons, a optimiser selon le gpu

    // Number of threads.
    this->numberOfThreads = dg.x * dg.y * dg.z * db.x * db.y * db.z;
    int numberOfThreadsPerBlock = db.x * db.y * db.z;

    // Power of 2! Depends on the number of threads per block.
    this->nSM = numberOfThreadsPerBlock;
    this->sizeSM = nSM * sizeof(float); // bytes.

    Device::gridHeuristic(dg, db);

    for (int iDevice = 0; iDevice < numberOfDevices; iDevice++)
	{
	HANDLE_ERROR(cudaSetDevice(iDevice));

	memoryManagement(iDevice);
	memoryManagementCuRand(iDevice);
	}

    }

Montecarlo::~Montecarlo(void)
    {
    delete[] tabMontecarlo;

    for (int i = 0; i < numberOfDevices; i++)
	{
	// Set the device.
	HANDLE_ERROR(cudaSetDevice(i));
	//MM (device free)
	    {
	    HANDLE_ERROR(cudaFree(ptrDevTabResult[i]));
	    HANDLE_ERROR(cudaFree(ptrDevTabGenerator[i]));

	    Device::lastCudaError("Montecarlo MM (end deallocation)"); // temp debug
	    }
	}

    }

/*--------------------------------------*\
 |*		Methode			*|
 \*-------------------------------------*/

float Montecarlo::getPi()
    {
    // pi : number of points under the function
    // Problem here!
    // Integral = n_o / (n_o + n_x) * (b - a) * M

    float M = yMax;
    float n_o = nrArrowsUnderCurve;
    float a = 1.0f;
/*
    cout << "M: " << yMax << endl;
    cout << "n_o: " << n_o << endl;
    cout << "n_x: " << n_x << endl;

    cout << "nr Arrows: " << numberOfRandomPoints << endl;
*/
    return (M * a) * n_o / (numberOfRandomPoints);
    }

void Montecarlo::memoryManagement(int iDevice)
    {
    // Memory Management
    // MM (malloc Device)
    HANDLE_ERROR(cudaMalloc(&ptrDevTabResult[iDevice], sizePI));

    // MM (memset Device)
    HANDLE_ERROR(cudaMemset(ptrDevTabResult[iDevice], 0, sizePI));

    // MM (copy Host->Device)
    //HANDLE_ERROR(cudaMemcpy(ptrDevResult, pi, sizePI, cudaMemcpyHostToDevice));

    // MM (copy Device->Host)
    //HANDLE_ERROR(cudaMemcpy(pi, ptrDevResult, sizePI, cudaMemcpyDeviceToHost));

    Device::lastCudaError("Montecarlo MM Result (end allocation)"); // temp debug
    }

void Montecarlo::memoryManagementCuRand(int iDevice)
    {
    size_t sizeDevTabGenerator = sizeof(curandState) * numberOfThreads;

    // Memory Management in case of multiGPU.
	{
	// MM (malloc Device)
	HANDLE_ERROR(cudaMalloc(&ptrDevTabGenerator[iDevice], sizeDevTabGenerator));

	// MM (memset Device)
	HANDLE_ERROR(cudaMemset(ptrDevTabGenerator[iDevice], 0, sizeDevTabGenerator));

	// MM (copy Host->Device)
	//HANDLE_ERROR(cudaMemcpy(ptrDevResult, pi, sizePI, cudaMemcpyHostToDevice));

	// MM (copy Device->Host)
	//HANDLE_ERROR(cudaMemcpy(pi, ptrDevResult, sizePI, cudaMemcpyDeviceToHost));

	// Startup setup kernel.
	// Most expensive computation.
    setup_kernel_rand<<<dg, db>>>(ptrDevTabGenerator[iDevice], Device::getDeviceId());
    }

Device::lastCudaError("Montecarlo MM CuRand Generators (end allocation)"); // temp debug
}

void Montecarlo::run()
{
Device::lastCudaError("Montecarlo (before)"); // temp debug
#pragma omp parallel for
for (int i = 0; i < numberOfDevices; i++)
    {
    HANDLE_ERROR(cudaSetDevice(i));

    kernelMontecarlo<<<dg, db, sizeSM>>>(ptrDevTabResult[i], ptrDevTabGenerator[i], (numberOfRandomPoints)/(numberOfDevices), nSM, yMax);// assynchrone

    Device::lastCudaError("Montecarlo (after)"); // temp debug

    //Device::synchronize(); // Temp, only for printf in  GPU

    // MM (copy Device -> Host)
	{
	HANDLE_ERROR(cudaMemcpy(&tabMontecarlo[i], ptrDevTabResult[i], sizePI, cudaMemcpyDeviceToHost)); // barriere synchronisation implicite
	}

    cout << "nr under curve: " << tabMontecarlo[i] << endl;
    // The tabMontecarlo[i] contains the number of points under the function.
#pragma omp atomic
    nrArrowsUnderCurve += tabMontecarlo[i];
    }

}

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
