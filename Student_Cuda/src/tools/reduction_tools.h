#pragma once

#include "cudaTools.h"
#include "MathTools.h"

template<typename T>
class ReductionTools
    {

    public:
	 /*
	 __device__
	 ReductionTools()
	 {

	 }

	 __device__
	 ~ReductionTools()
	 {

	 }
	 */

	__device__
	//template<typename T>
	static void reduce(T* arraySharedMemory, T* ptrDeviceResult, int numberOfElements)
	    {
	    // Populate GM?
	    //reduceIntraThread(arraySharedMemory, ptrDeviceArrayGlobalMemory, numberOfElementsGlobalMemory, numberOfElementsSharedMemory);

	    __syncthreads();
	    reduceIntraBlock(arraySharedMemory, numberOfElements);
	    reduceInterBlock(arraySharedMemory, ptrDeviceResult);
	    __syncthreads();

	    //work(ptrDeviceArrayGlobalMemory);
	    }

    private:
	/**
	 * Write stuff in shared memory and reduce at the same time.
	 */
	__device__
	//template<typename T>
	void reduceIntraThread(T* arraySharedMemory, T* ptrDeviceArrayGlobalMemory, int numberOfElementsGlobalMemory, int numberOfElementsSharedMemory)
	    {
	    int TID = Indice2D::tid();
	    int TID_LOCAL = Indice2D::tidLocal();
	    int NB_THREADS = Indice2D::nbThread();

	    int s = TID;

	    // Value stored in registry.
	    T sumThread = 0;
	    while (s < numberOfElementsGlobalMemory)
		{
		sumThread += ptrDeviceArrayGlobalMemory[s]; // We can also compute here instead of reading the GM and avoid memory access.

		s += NB_THREADS;
		}

	    // Write in this thread's block SM.
	    // The thread only has acces to this block's array stored in Shared Memory.
	    arraySharedMemory[TID_LOCAL] = sumThread;
	    }

	__device__
	//template<typename T>
	static void reduceIntraBlock(T* arraySharedMemory, int numberOfElements)
	    {

	    int middleIndex = numberOfElements / 2;
	    while (middleIndex >= 1)
		{
		replace(arraySharedMemory, middleIndex);
		middleIndex /= 2;
		}
	    }

	__device__
	//template<typename T>
	static void reduceInterBlock(T* arraySharedMemory, T* ptrDeviceResult)
	    {
	    if (threadIdx.x == 0)
		{
		atomicAdd(ptrDeviceResult, arraySharedMemory[0]);
		}
	    }

	__device__
	//template<typename T>
	static void replace(T* arraySharedMemory, int middleIndex)
	    {
	    int TID_LOCAL = Indice2D::tidLocal();
	    int NB_THREADS_LOCAL = Indice2D::nbThreadLocal();

	    int workingThreadID = TID_LOCAL;

	    while (workingThreadID < middleIndex)
		{
		//atomicAdd(&arraySharedMemory[workingThreadID],arraySharedMemory[workingThreadID] + arraySharedMemory[workingThreadID + middleIndex]);
		arraySharedMemory[workingThreadID] = arraySharedMemory[workingThreadID] + arraySharedMemory[workingThreadID + middleIndex];

		workingThreadID += NB_THREADS_LOCAL;
		}
	    }
    };
