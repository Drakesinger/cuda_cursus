#include <iostream>

#include "Device.h"
#include "Histogram.h"

#include <algorithm>

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void kernelHistogram(int* ptrDevResult, int* ptrDevTabInput, int n, int nSM);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Constructeur			*|
 \*-------------------------------------*/

Histogram::Histogram(int n) :
	n(n)
    {
    // Set up min and max.
    this->minValue = 0;
    this->maxValue = 256;

    this->numberOfDevices = 1; //Device::getDeviceCount();

    // This will contain the histogram.
    this->tabHistogram = new int*[numberOfDevices];

    this->ptrTabInputNumbers = new int*[numberOfDevices];

    this->ptrDevTabResult = new int*[numberOfDevices];

    // Grid.
    this->dg = dim3(32, 16, 1); // disons, a optimiser selon le gpu
    this->db = dim3(512, 1, 1); // disons, a optimiser selon le gpu

    // Number of threads.
    this->numberOfThreads = dg.x * dg.y * dg.z * db.x * db.y * db.z;
    int numberOfThreadsPerBlock = db.x * db.y * db.z;

    // Power of 2! Depends on the number of threads per block.
    this->nSM = numberOfThreadsPerBlock;
    this->sizeSM = nSM * sizeof(int); // bytes.

    this->sizeTabInput = n * sizeof(int); // bytes

    Device::gridHeuristic(dg, db);

    generateNumbers();

    for (int j = 0; j < numberOfDevices; j++)
	{
	tabHistogram[j] = new int[maxValue];
	for (int i = 0; i < maxValue; ++i)
	    {
	    tabHistogram[i] = 0;
	    }
	}
    // Memory management for multi-gpu.
    for (int iDevice = 0; iDevice < numberOfDevices; iDevice++)
	{
	HANDLE_ERROR(cudaSetDevice(iDevice));

	cout << "Launching memory management for Device " << iDevice << endl;

	memoryManagement(iDevice);
	}

    }

Histogram::~Histogram(void)
    {
    delete[] tabHistogram;

    for (int i = 0; i < numberOfDevices; i++)
	{
	// Set the device.
	HANDLE_ERROR(cudaSetDevice(i));
	//MM (device free)
	    {
	    HANDLE_ERROR(cudaFree(ptrDevTabResult[i]));
	    HANDLE_ERROR(cudaFree(ptrDevTabInput[i]));

	    Device::lastCudaError("Histogram MM (end deallocation)"); // temp debug
	    }
	}

    }

/*--------------------------------------*\
 |*		Methode			*|
 \*-------------------------------------*/

int* Histogram::getHistogram()
    {
    return tabHistogram;
    }

void Histogram::memoryManagement(int iDevice)
    {
    // Memory Management
    // MM (malloc Device)
    HANDLE_ERROR(cudaMalloc(&ptrDevTabResult[iDevice], sizeSM));
    HANDLE_ERROR(cudaMalloc(&ptrDevTabInput[iDevice], sizeTabInput));

    // MM (memset Device)
    HANDLE_ERROR(cudaMemset(ptrDevTabResult[iDevice], 0, sizeSM));

    // MM (copy Host->Device)
    HANDLE_ERROR(cudaMemcpy(ptrDevTabInput, ptrTabInputNumbers, sizeTabInput, cudaMemcpyHostToDevice));

    // MM (copy Device->Host)
    //HANDLE_ERROR(cudaMemcpy(pi, ptrDevResult, sizePI, cudaMemcpyDeviceToHost));

    Device::lastCudaError("Histogram MM Result (end allocation)"); // temp debug
    }

void Histogram::generateNumbers()
    {

    for (int i = 0; i < numberOfDevices; i++)
	{
	ptrTabInputNumbers[i] = new int[n];
	for (int j = 0; j < n; j++)
	    ptrTabInputNumbers[i][j] = j % maxValue;
	}

    // Randomize the array contents.
    for (int i = 0; i < numberOfDevices; i++)
	{
	for (int j = 0; j < n; j++)
	    std::swap(ptrTabInputNumbers[i][randomMinMax(0, n - 1)], ptrTabInputNumbers[i][randomMinMax(0, n - 1)]);
	}

    }

int Histogram::randomMinMax(int min, int max)
    {
    return (int) ((max - min) * ((float) (rand()) / (float) (RAND_MAX)) + min);
    }

void Histogram::run()
    {
    Device::lastCudaError("Histogram (before)"); // temp debug
#pragma omp parallel for
    for (int i = 0; i < numberOfDevices; i++)
	{
	HANDLE_ERROR(cudaSetDevice(i));

	kernelHistogram<<<dg, db, sizeSM>>>(ptrDevTabResult[i], ptrDevTabInput[i], n, nSM); // assynchrone

	Device::lastCudaError("Histogram (after)"); // temp debug

	Device::synchronize(); // Temp, only for printf in  GPU

// MM (copy Device -> Host)
	    {
	    HANDLE_ERROR(cudaMemcpy(&tabHistogram[i], ptrDevTabResult[i], sizeSM, cudaMemcpyDeviceToHost)); // barriere synchronisation implicite
	    }

// The tabMontecarlo[i] contains the number of points under the function.
//#pragma omp atomic
	//nrArrowsUnderCurve += tabMontecarlo[i];
	}

    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
