#pragma once

#include "curand_kernel.h"
#include "cudaTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Histogram
    {
	/*--------------------------------------*\
	|*		Constructor		*|
	 \*-------------------------------------*/

    public:

	Histogram(int n);

	virtual ~Histogram(void);

	/*--------------------------------------*\
	|*		Methodes		*|
	 \*-------------------------------------*/

    public:

	void run();
	float getPi();

    private:
	void memoryManagement(int iDevice);
	void memoryManagementCuRand(int iDevice);
	int randomMinMax(int min, int max);
	void generateNumbers();

	/*--------------------------------------*\
	|*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int n;

	// Outputs
	int* tabHistogram;

	// Tools
	dim3 dg;
	dim3 db;

	int minValue;
	int maxValue;

	// Number of slots in SharedMemory.
	int nSM;

	int numberOfDevices;
	int numberOfThreads;

	// Array of numbers to randomize and turn into histogram.
	int* ptrTabInputNumbers;

	// Array that will contain the cuda random number generators.
	curandState** ptrDevTabGenerator;

	// Array that will store the result on the device.
	int** ptrDevTabResult;

	// Array that will store the random n numbers on the device.
	int** ptrDevTabInput;

	// Size of shared array in SHARED MEMORY.
	size_t sizeSM;

	// Size of the array of numbers.
	size_T sizeTabInput;

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
