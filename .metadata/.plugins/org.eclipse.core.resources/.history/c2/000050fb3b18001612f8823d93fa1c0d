#include <iostream>

#include "Device.h"
#include "Slice.h"

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void kernelPI(float* ptrDevResult, int nrSlices, int nSM);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Constructeur			*|
 \*-------------------------------------*/

Slice::Slice(int nrSlices) :
	nrSlices(nrSlices)
    {

    // Grid
	{
	this->dg = dim3(36, 16, 1); // disons, a optimiser selon le gpu
	this->db = dim3(512, 1, 1); // disons, a optimiser selon le gpu

	Device::gridHeuristic(dg, db);
	}

    this->sizeSM = nSM * sizeof(float); // octet
    this->pi = 0.0f;

    memoryManagement();
    }

Slice::~Slice(void)
    {
    //MM (device free)
	{
	HANDLE_ERROR(cudaFree(ptrDevResult));

	Device::lastCudaError("AddVector MM (end deallocation)"); // temp debug
	}
    }

/*--------------------------------------*\
 |*		Methode			*|
 \*-------------------------------------*/

float Slice::getPi()
    {
    return (pi) / nrSlices;
    }

void Slice::memoryManagement()
    {
    ptrDevResult = NULL;

    sizePI = sizeof(float);

    // Memory Management

    // MM (malloc Device)
	{
	HANDLE_ERROR(cudaMalloc(&ptrDevResult, sizePI));
	}

    // MM (memset Device)
	{
	HANDLE_ERROR(cudaMemset(ptrDevResult, 0, sizePI));
	}

    // MM (copy Host->Device)
	{
	//HANDLE_ERROR(cudaMemcpy(ptrDevResult, pi, sizePI, cudaMemcpyHostToDevice));
	}

    // MM (copy Device->Host)
	{
	//HANDLE_ERROR(cudaMemcpy(pi, ptrDevResult, sizePI, cudaMemcpyDeviceToHost));
	}

    Device::lastCudaError("SLICE MM (end allocation)"); // temp debug
    }

void Slice::run()
    {
    Device::lastCudaError("Slice (before)"); // temp debug

    kernelPI<<<dg, db, sizeSM>>>(ptrDevResult, nrSlices, nSM); // assynchrone

    Device::lastCudaError("Slice (after)"); // temp debug

    Device::synchronize(); // Temp, only for printf in  GPU

    // MM (copy Device -> Host)
	{
	HANDLE_ERROR(cudaMemcpy(&pi, ptrDevResult, sizePI, cudaMemcpyDeviceToHost)); // barriere synchronisation implicite
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
