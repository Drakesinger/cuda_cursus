#include "Indice2D.h"
#include "cudaTools.h"
#include "reduction_tools.h"

#include <curand_kernel.h>

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void kernelHistogram(int* ptrDevResult, int* ptrDevTabInput, int n, int nSM);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void initializeSMM(int* tabSM, int nSM);
__device__ void reduceIntraThread(int* tabSM, int* ptrDevTabInput, int n);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * output : void required !!
 */
__global__ void kernelHistogram(int* ptrDevResult, int* ptrDevTabInput, int n, int nSM)
    {
    extern __shared__ int tabSM[];

/*
    int tid = threadIdx.x + gridDim.x * blockIdx.x;
    const int NB_THREADS = gridDim.x * blockDim.x;
    int s = tid;

    if(threadIdx.x < nSM)
	tabSM[threadIdx.x] = 0;

    __syncthreads();

    while(s < n)
	{
	atomicAdd(&tabSM[ptrDevTabInput[s]], 1);
	s += NB_THREADS;
	}

    __syncthreads();

    if(threadIdx.x < nSM)
	atomicAdd(&ptrDevResult[threadIdx.x], tabSM[threadIdx.x]);
*/
    //Initialize Shared Memory.
    //initializeSMM(tabSM, nSM);

    __syncthreads();
    reduceIntraThread(tabSM, ptrDevTabInput, n);
    __syncthreads();

    // Call to ReuctionTools.reduce<float>
    //ReductionTools<int>::reduce(tabSM, ptrDevResult, nSM);

    //__syncthreads();
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void reduceIntraThread(int* tabSM, int* ptrDevTabInput, int n)
    {
    const int NB_THREADS = Indice2D::nbThread();
    const int TID = Indice2D::tid();
    const int TID_LOCAL = Indice2D::tidLocal();

    // Working ThreadID;
    int s = TID;

    while (s < n)
	{
	if(ptrDevTabInput[s] < 256)
	    {
	// Add a 1 to the correct spot in the shared memory.
	atomicAdd(&tabSM[ptrDevTabInput[s]], 0);
	    }
	// Jump.
	s += NB_THREADS;
	}
    // No consolidation here.
    }

__device__ void initializeSMM(int* tabSM, int nSM)
    {
    // Put all SharedMemory to 0.
    const int NB_THREAD_BLOCK = Indice2D::nbThreadBlock();
    const int TID_LOCAL = Indice2D::tidLocal();
    int s = TID_LOCAL;

    while (s < nSM)
	{
	tabSM[s] = 0;
	s += NB_THREAD_BLOCK;
	}
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

