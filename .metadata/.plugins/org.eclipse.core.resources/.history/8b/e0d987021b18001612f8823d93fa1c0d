#pragma once

#include "curand_kernel.h"
#include "cudaTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Montecarlo
    {
	/*--------------------------------------*\
	|*		Constructor		*|
	 \*-------------------------------------*/

    public:

	Montecarlo(int nrSlices, int nSM, int numberOfRandomPoints, int yMax);

	virtual ~Montecarlo(void);

	/*--------------------------------------*\
	|*		Methodes		*|
	 \*-------------------------------------*/

    public:

	void run();
	float getPi();

    private:
	void memoryManagement();
	void memoryManagementCuRand();

	/*--------------------------------------*\
	|*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int nrSlices;
	int numberOfRandomPoints;
	float yMax;

	// Outputs
	float pi;
	float* tabMontecarlo;

	// Tools
	dim3 dg;
	dim3 db;

	int nSM;
	int numberOfDevices;

	// Array that will contain the cuda random number generators.
	curandState* ptrDevTabGenerator;

	// Array that will store the result on the device.
	float* ptrDevResult;
	// Size of shared array in SHARED MEMORY.
	size_t sizeSM;
	// Size of PI in SHARED MEMORY.
	size_t sizePI;

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
