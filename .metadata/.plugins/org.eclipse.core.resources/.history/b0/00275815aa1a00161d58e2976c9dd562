#include <iostream>

#include "Device.h"
#include "Histogram.h"

#include <algorithm>

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

extern __global__ void kernelHistogram(int* ptrDevResult, int* ptrDevTabInput, int n, int nSM);

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Constructeur			*|
 \*-------------------------------------*/

Histogram::Histogram(int n) :
	n(n)
    {
    // Set up min and max.
    this->minValue = 0;
    this->maxValue = 256;

    this->tabHistogram = new int[maxValue];
    this->ptrTabInputNumbers = new int[n];
    this->ptrDevTabResult = new int[maxValue];

    // Grid.
    this->dg = dim3(32, 16, 1); // disons, a optimiser selon le gpu
    this->db = dim3(512, 1, 1); // disons, a optimiser selon le gpu

    // Number of threads.
    this->numberOfThreads = dg.x * dg.y * dg.z * db.x * db.y * db.z;
    int numberOfThreadsPerBlock = db.x * db.y * db.z;

    // Power of 2! Depends on the number of threads per block.
    this->nSM = numberOfThreadsPerBlock;
    this->sizeSM = nSM * sizeof(int); // bytes.

    this->sizeTabInput = n * sizeof(int); // bytes

    Device::gridHeuristic(dg, db);

    generateNumbers();

    for (int i = 0; i < maxValue; ++i)
	{
	tabHistogram[i] = 0;
	}

    memoryManagement();

    }

Histogram::~Histogram(void)
    {
    delete[] tabHistogram;

	{
	//MM (device free)
	    {
	    HANDLE_ERROR(cudaFree(ptrDevTabResult));
	    HANDLE_ERROR(cudaFree(ptrDevTabInput));

	    Device::lastCudaError("Histogram MM (end deallocation)"); // temp debug
	    }
	}

    }

/*--------------------------------------*\
 |*		Methode			*|
 \*-------------------------------------*/

int* Histogram::getHistogram()
    {
    return tabHistogram;
    }

void Histogram::memoryManagement()
    {
    // Memory Management
    // MM (malloc Device)
    HANDLE_ERROR(cudaMalloc(&ptrDevTabResult, sizeSM));
    HANDLE_ERROR(cudaMalloc(&ptrDevTabInput, sizeTabInput));

    // MM (memset Device)
    HANDLE_ERROR(cudaMemset(ptrDevTabResult, 0, sizeSM));

    // MM (copy Host->Device)
    HANDLE_ERROR(cudaMemcpy(ptrDevTabInput, ptrTabInputNumbers, sizeTabInput, cudaMemcpyHostToDevice));

    // MM (copy Device->Host)
    //HANDLE_ERROR(cudaMemcpy(pi, ptrDevResult, sizePI, cudaMemcpyDeviceToHost));

    Device::lastCudaError("Histogram MM Result (end allocation)"); // temp debug
    }

void Histogram::generateNumbers()
    {

    for (int j = 0; j < n; j++)
	{
	ptrTabInputNumbers[j] = j % maxValue;
	}

    // Randomize the array contents.
    for (int i = 0; i < n; i++)
	{
	std::swap(ptrTabInputNumbers[randomMinMax(0, n - 1)], ptrTabInputNumbers[randomMinMax(0, n - 1)]);
	}
    }

int Histogram::randomMinMax(int min, int max)
    {
    return (int) ((max - min) * ((float) (rand()) / (float) (RAND_MAX)) + min);
    }

void Histogram::run()
    {
    Device::lastCudaError("Histogram (before)"); // temp debug

	{

	kernelHistogram<<<dg, db, sizeSM>>>(ptrDevTabResult, ptrDevTabInput, n, nSM); // assynchrone

	Device::lastCudaError("Histogram (after)"); // temp debug

	Device::synchronize(); // Temp, only for printf in  GPU

// MM (copy Device -> Host)
	    {
	    HANDLE_ERROR(cudaMemcpy(&tabHistogram, ptrDevTabResult, sizeSM, cudaMemcpyDeviceToHost)); // barriere synchronisation implicite
	    }

// The tabMontecarlo[i] contains the number of points under the function.
//#pragma omp atomic
	//nrArrowsUnderCurve += tabMontecarlo[i];
	}

    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
