#pragma once

#include "curand_kernel.h"
#include "cudaTools.h"

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Montecarlo
    {
	/*--------------------------------------*\
	|*		Constructor		*|
	 \*-------------------------------------*/

    public:

	Montecarlo(int nSM, int numberOfRandomPoints, int yMax);

	virtual ~Montecarlo(void);

	/*--------------------------------------*\
	|*		Methodes		*|
	 \*-------------------------------------*/

    public:

	void run();
	float getPi();

    private:
	void memoryManagement(int iDevice);
	void memoryManagementCuRand(int iDevice);

	/*--------------------------------------*\
	|*		Attributs		*|
	 \*-------------------------------------*/

    private:

	// Inputs
	int numberOfRandomPoints;
	float yMax;

	// Outputs
	float* tabMontecarlo;
	float nrArrowsUnderCurve;

	// Tools
	dim3 dg;
	dim3 db;

	int nSM;
	int numberOfDevices;
	int numberOfThreads;

	// Array that will contain the cuda random number generators.
	curandState** ptrDevTabGenerator;

	// Array that will store the result on the device.
	float** ptrDevTabResult;

	// Size of shared array in SHARED MEMORY.
	size_t sizeSM;
	// Size of PI in SHARED MEMORY.
	size_t sizePI;

    };

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
