#include "Indice2D.h"
#include "cudaTools.h"
#include "reduction_tools.h"

#include <curand_kernel.h>
//#include <curandTools.h>

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Imported	 	*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

__global__ void kernelMontecarlo(float* ptrDevResult, curandState* ptrDevTabGenerator, int nrSlices, int nSM, float yMax);

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void initializeSMM(float* tabSM, int nSM);
__device__ void workM(float* tabSM, curandState* ptrDevTabGenerator, int nrSlices, float yMax);
__device__ float fpiM(float x);

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * output : void required !!
 */
__global__ void kernelMontecarlo(float* ptrDevResult, curandState* ptrDevTabGenerator, int nrSlices, int nSM, float yMax)
    {
    extern __shared__ float tabSM[];
    // Initialize ReductionTools if needed.

    //Initialize Shared Memory.
    initializeSMM(tabSM, nSM);
    __syncthreads();
    workM(tabSM, ptrDevTabGenerator, nrSlices, yMax);
    __syncthreads();
    // Call to ReuctionTools.reduce<float>
    ReductionTools<float>::reduce(tabSM, ptrDevResult, nSM);
    //__syncthreads();
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

__device__ void workM(float* tabSM, curandState* ptrDevTabGenerator, int nrSlices, float yMax)
    {
    const int NB_THREADS = Indice2D::nbThread();
    const int TID = Indice2D::tid();
    const int TID_LOCAL = Indice2D::tidLocal();

    int s = TID;

    //float dx = 1.0f / float(nrSlices);
    curandState state = ptrDevTabGenerator[TID];

    // Global sum;
    float sum_thread = 0.0f;

    while (s < nrSlices)
	{
	float x = curand_uniform(&state);
	float y = curand_uniform(&state) * yMax;

	// If y is smaller than the function:
	float fx = fpiM(x);
	//float xs = s * dx;
	if (y > fx)
	    {
	    sum_thread += 1; //fpiM(xs);
	    }
	// Jump.
	s += NB_THREADS;
	}

    tabSM[TID_LOCAL] = sum_thread;
    }

__device__ float fpiM(float x)
    {
    return 4.0f / (1.0f + x * x);
    }

__device__ void initializeSMM(float* tabSM, int nSM)
    {
    // Put all SharedMemory to 0.
    const int NB_THREAD_BLOCK = Indice2D::nbThreadBlock();
    const int TID_LOCAL = Indice2D::tidLocal();
    int s = TID_LOCAL;

    while (s < nSM)
	{
	tabSM[s] = 0.0f;
	s += NB_THREAD_BLOCK;
	}
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

