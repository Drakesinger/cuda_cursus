#include "Fractal.h"
#include "FractalMath.h"

#include <iostream>
#include <omp.h>
#include "OmpTools.h"

#include "IndiceTools_CPU.h"
using cpu::IndiceTools;

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

Fractal::Fractal(uint w, uint h, float dt, int N, const DomaineMath& domaineMath) :
	Animable_I<uchar4>(w, h, "Fractal_OMP_rgba_uchar4", domaineMath), variateurAnimation(Interval<float>(0, 1.5), dt)
    {
    // Input
    this->dt = dt;  // animation
    this->N = N;

    // Tools
    this->t = 0;					// protected dans super classe Animable
    this->parallelPatern = ParallelPatern::OMP_MIXTE;   // protected dans super classe Animable

    // OMP
    cout << "\n[Fractal] : OMP : nbThread = " << this->nbThread << endl; // protected dans super classe Animable
    }

Fractal::~Fractal(void)
    {
    // Nothing.
    }

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/**
 * Override
 */
void Fractal::animationStep()
    {
    this->t += dt + variateurAnimation.varierAndGet(); //dt;
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

void Fractal::processEntrelacementOMP(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath)
    {

    // Start up our math object.
    FractalMath fractalMath = FractalMath(w);

    const int WH = w * h;

#pragma omp parallel
	{
	// The system has initiated the threads so this info is now known. Outside of the parallel region, it is not.
	const int NB_THREAD = OmpTools::getNbThread(); // dans region parallel

	const int TID = OmpTools::getTid();
	int s = TID; // in [0,...

	// Image domain.
	int i;
	int j;

	while (s < WH)
	    {
	    IndiceTools::toIJ(s, w, &i, &j);   // s[0,W*H[ --> i[0,H[ j[0,W[

	    workPixel(&ptrTabPixels[s], i, j, domaineMath, &fractalMath);

	    s += NB_THREAD;
	    }
	}

    }

void Fractal::processForAutoOMP(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath)
    {

    // Start up our math object.
    FractalMath fractalMath = FractalMath(w);

#pragma omp parallel for
    for (int i = 0; i < w; i++) // Rows.
	{
	for (int j = 0; j < h; j++) // Columns.
	    {

	    int s = IndiceTools::toS(w, i, j);    // i[0,H[ j[0,W[  --> s[0,W*H[
	    workPixel(&ptrTabPixels[s], i, j, domaineMath, &fractalMath);

	    }
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * i in [0,h[
 * j in [0,w[
 *
 * code commun a:
 * 	- entrelacementOMP
 * 	- forAutoOMP
 */
void Fractal::workPixel(uchar4* ptrColorIJ, int i, int j, const DomaineMath& domaineMath, FractalMath* ptrFractalMath)
    {
    // (i,j) domaine ecran dans N2
    // (x,y) domaine math dans R2

    // Fractal domain.
    double x;
    double y;

    domaineMath.toXY(i, j, &x, &y); // fill (x,y) from (i,j)

    // float t=variateurAnimation.get(); test
    ptrFractalMath->computeGeometricSuite(ptrColorIJ, x, y, t, this->N);
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
