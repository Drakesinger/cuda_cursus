#pragma once

#include "JuliaMath.h"

#include "cudaType_CPU.h"
#include "Fractal.h"
#include "Animable_I_CPU.h"
using namespace cpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class Julia: public Fractal
    {

	 /*-------------------------------------*\
	 |*		Constructor		*|
	 \*-------------------------------------*/

    public:

	Julia(uint w, uint h, float dt, float c_re, float c_im, int N, const DomaineMath& domaineMath);

	virtual ~Julia(void);

    public:

    	/*-------------------------*\
    	|*   Override Animable_I   *|
    	 \*------------------------*/


    	/**
    	 * Call periodicly by the api
    	 * Image non zoomable : domaineMath pas use ici
    	 */
    	virtual void processEntrelacementOMP(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath);

    	/**
    	 * Call periodicly by the api
    	 * Image non zoomable : domaineMath pas use ici
    	 */
    	virtual void processForAutoOMP(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath);

    	/**
    	 * Call periodicly by the api
    	 */
    	virtual void animationStep();

    private:

    	/**
    	 * i in [0,h[
    	 * j in [0,w[
    	 *
    	 * code commun a:
    	 * 	- entrelacementOMP
    	 * 	- forAutoOMP
    	 */
    	void workPixel(uchar4* ptrColorIJ, int i, int j, const DomaineMath& domaineMath, JuliaMath* ptrFractalMath);

    	/*--------------------------------------*\
    	|*		Attribut		*|
    	 \*-------------------------------------*/

        private:

    	// Inputs
    	double dt;
        float c_im;
        float c_re;


    };


/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
