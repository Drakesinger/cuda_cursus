#pragma once

#include <math.h>
#include "MathTools.h"

#include "ColorTools_CPU.h"
using namespace cpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

class JuliaMath
{
	/*--------------------------------------*\
	|*		Constructeur		*|
	\*-------------------------------------*/

public:

	JuliaMath(uint w)
	{
		this->dim2 = w / 2;
	}

	// constructeur copie: pas besoin car pas attribut ptr

	virtual ~JuliaMath(void)
	{
		// rien
	}

	/*--------------------------------------*\
	|*		Methode			*|
	\*-------------------------------------*/

public:

	void computeGeometricSuite(uchar4* ptrColorIJ, double x, double y, float t, int N, float c_re, float c_im)
	{
		double a = x;
		double b = y;
		int k = 0;

		do
		{
			double a_copy = a;
			a = (a*a - b*b) + c_re;
			b = 2 * a_copy*b + c_im;

			if (a*a + b*b > 4.0)
			{
				colorXY(ptrColorIJ, determineProRata(k, N));
				break;
			}

			colorXYBlack(ptrColorIJ);

			k++;
		} while (k <= N);
	}

private:

	float determineProRata(int s, int N)
	{
		return float(s) / float(N);
	}

	void colorXYBlack(uchar4* ptrColorIJ)
	{
		// The pixel @ (x,y) belongs to the fractal.
		// We need to color it in black.

		ptrColorIJ->x = 0;
		ptrColorIJ->y = 0;
		ptrColorIJ->z = 0;

		ptrColorIJ->w = 255; // Opaque.
	}

	void colorXY(uchar4* ptrColorIJ, float hue)
	{
		// The pixel @ (x,y) does not belong to the fractal.
		// It's color depends on the 1st k<=N for which norm(z_k) > 2

		ColorTools::HSB_TO_RVB(hue, ptrColorIJ);
	}

	/*--------------------------------------*\
	|*		Attribut			*|
	\*-------------------------------------*/

private:

	// Tools
	double dim2;
};

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
