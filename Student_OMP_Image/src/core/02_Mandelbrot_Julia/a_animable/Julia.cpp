#include "Julia.h"
#include "JuliaMath.h"

#include <iostream>
#include <omp.h>
#include "OmpTools.h"

#include "IndiceTools_CPU.h"
using cpu::IndiceTools;

using std::cout;
using std::endl;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

Julia::Julia(uint w, uint h, float dt, float C_real, float C_imaginary, int N, const DomaineMath& domaineMath) :
	Fractal(w, h, dt, N, domaineMath)
    {
    // Input
    this->dt = dt;  // animation

    this->c_im = C_imaginary;
    this->c_re = C_real;

    // Tools
    this->t = 0;					// protected dans super classe Animable
    this->parallelPatern = ParallelPatern::OMP_MIXTE;   // protected dans super classe Animable

    // OMP
    cout << "\n[Julia] : OMP : nbThread = " << this->nbThread << endl; // protected dans super classe Animable
    }

Julia::~Julia(void)
    {
    // Nothing.
    }

void Julia::animationStep()
    {
    t += dt;
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

void Julia::processEntrelacementOMP(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath)
    {
    // Start up our math object.
    JuliaMath fractalMath = JuliaMath(w);

    const int WH = w * h;

#pragma omp parallel
	{
	// The system has initiated the threads so this info is now known. Outside of the parallel region, it is not.
	const int NB_THREAD = OmpTools::getNbThread(); // dans region parallel

	const int TID = OmpTools::getTid();
	int s = TID; // in [0,...

	// Image domain.
	int i;
	int j;

	while (s < WH)
	    {
	    IndiceTools::toIJ(s, w, &i, &j);   // s[0,W*H[ --> i[0,H[ j[0,W[
	    workPixel(&ptrTabPixels[s], i, j, domaineMath, &fractalMath);
	    s += NB_THREAD;
	    }
	}
    }

void Julia::processForAutoOMP(uchar4* ptrTabPixels, uint w, uint h, const DomaineMath& domaineMath)
    {
    // Start up our math object.
    JuliaMath fractalMath = JuliaMath(w);

#pragma omp parallel for
    for (int i = 0; i < w; i++) // Rows.
	{
	for (int j = 0; j < h; j++) // Columns.
	    {
	    int s = IndiceTools::toS(w, i, j);    // i[0,H[ j[0,W[  --> s[0,W*H[
	    workPixel(&ptrTabPixels[s], i, j, domaineMath, &fractalMath);
	    }
	}
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/**
 * i in [0,h[
 * j in [0,w[
 *
 * code commun a:
 * 	- entrelacementOMP
 * 	- forAutoOMP
 */
void Julia::workPixel(uchar4* ptrColorIJ, int i, int j, const DomaineMath& domaineMath, JuliaMath* ptrFractalMath)
    {
    // (i,j) domaine ecran dans N2
    // (x,y) domaine math dans R2

    // Fractal domain.
    double x;
    double y;

    domaineMath.toXY(i, j, &x, &y); // fill (x,y) from (i,j)

    // float t=variateurAnimation.get(); test
    ptrFractalMath->computeGeometricSuite(ptrColorIJ, x, y, t, this->N, this->c_re, this->c_im);
    }

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/
