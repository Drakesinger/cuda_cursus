#include "FractalProvider.h"
#include "Fractal.h"
#include "Julia.h"

#include "MathTools.h"

#include "ImageAnimable_CPU.h"
#include "DomaineMath_CPU.h"
using namespace cpu;

/*----------------------------------------------------------------------*\
 |*			Declaration 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			Implementation 					*|
 \*---------------------------------------------------------------------*/

/*--------------------------------------*\
 |*		Public			*|
 \*-------------------------------------*/

/*--------------------------------------*\
 |*		Surcharge		*|
 \*-------------------------------------*/

/**
 * Override
 */
Animable_I_uchar4* FractalProvider::createAnimable(void)
    {
    DomaineMath domaineMath = DomaineMath(-1.5, -1.5, 1.5, 1.5);

    // Animation
    float dt = 1;

    // Dimension
    int dw = 16 * 60;
    int dh = 16 * 60;
    
    // Limit.
    int N = 50;


    Fractal* fractale = new Julia(dw,dh,dt,0.25,0.789, N, domaineMath);
    //Fractal* fractale = new Fractal(dw, dh, dt, N, domaineMath);
    
    return fractale;
    }

/**
 * Override
 */
Image_I* FractalProvider::createImageGL(void)
    {
    ColorRGB_01 colorTexte(0, 1, 0); // green
    return new ImageAnimable_RGBA_uchar4(createAnimable(), colorTexte);
    }

/*--------------------------------------*\
 |*		Private			*|
 \*-------------------------------------*/

/*----------------------------------------------------------------------*\
 |*			End	 					*|
 \*---------------------------------------------------------------------*/

